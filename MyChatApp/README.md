# My Chat App - Android chat app base on Firebase.  
My Chat App is a example for chat app base on Google Firebase, messages are delivered in real-time.  
Use tool: Androi Studio 2.3.2
Use SDK: -MinSdkVersion 19
         -TargetSdkVersion 25
Steps run my project: + Install Android Studio 2.3.2
                      + Dowload source code from bitbucket with link https://DinhThanhHa@bitbucket.org/hadinh-internship/android-project.git
                      + Open project in Android Studio
                      + Connection Genymotion or mobile phone(You must sure install full service of google as: google+, CHplay...)
                      + Set up and initialize Firebase SDK for Cloud Functions(If you want to edit it)
                        - Link: https://firebase.google.com/docs/functions/get-started
                        - Run into teminal: npm install -g firebase-tools 
                        - Next: Run firebase login to log in via the browser and authenticate the firebase tool.
                                Go to your Firebase project directory(android-project/MyChatApp/notificationfunction).
                                Run firebase init functions. The tool gives you an option to install dependencies with npm. It is safe to decline if you want to manage dependencies in another way.              
                      + You must sure have internet
                      + Finally, build app and run on mobile phone or genymotion...
### Feature  
**1. Signin, signout, register.**  
* Register: allow user register a new account with username, email and password
* Signin: user must signin with email and password to use app.
* Edit profile: change avatar image, edit status.

**2. Chat**  
* Chat with friend: add friend, unfriend, send text message.

**3. Notification**  
* Push notification on status bar when has new message.  

### ScreenShots
* Update late.....
