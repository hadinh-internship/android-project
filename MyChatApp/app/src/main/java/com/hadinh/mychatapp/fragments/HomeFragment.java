package com.hadinh.mychatapp.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.models.Post;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.text.DateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final int GALLERY_PICK = 1;
    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;
    private ImageView imageProfileHome;
    private EditText txtPost;
    private Button btnPost;
    private String uid;
    private DatabaseReference mRoot;
    private DatabaseReference mPostUserDatabase;
    private RecyclerView mPostList;
    private DatabaseReference mUserRoot;
    private ImageView postImage;
    private FirebaseUser mCurrentUser;
    private int count = 0;

    private StorageReference mImageStorage;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        imageProfileHome = (ImageView) view.findViewById(R.id.imageProfileHome);
        txtPost = (EditText) view.findViewById(R.id.txtPost);
        btnPost = (Button) view.findViewById(R.id.btnPost);
        postImage = (ImageView) view.findViewById(R.id.imgPost);
        mAuth = FirebaseAuth.getInstance();
        mPostList = (RecyclerView) view.findViewById(R.id.recycler_view_post);
        uid = mAuth.getCurrentUser().getUid();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Post").child(uid);
        mPostUserDatabase = FirebaseDatabase.getInstance().getReference().child("Post");
        mRoot = FirebaseDatabase.getInstance().getReference().child("Users");
        mUserRoot = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mImageStorage = FirebaseStorage.getInstance().getReference();
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String post_status = txtPost.getText().toString();
                if (!post_status.isEmpty()) {
                    final String currentDate = DateFormat.getDateTimeInstance().format(new Date());
                    mUserDatabase.child("postStatus").setValue(post_status);
                    mUserDatabase.child("time_post").setValue(currentDate);
                    mUserDatabase.child("countLike").setValue(0);
                    txtPost.setText("");
                }
            }
        });

        postImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, GALLERY_PICK);
            }
        });
        mUserRoot.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String image = dataSnapshot.child("thumb_image").getValue().toString();
                Picasso.with(getContext()).load(image).placeholder(R.drawable.avata_icon).into(imageProfileHome);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mPostList.setHasFixedSize(true);
        mPostList.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == GALLERY_PICK && resultCode == Activity.RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1, 1)
                    .setMinCropWindowSize(500, 500)
                    .start(getActivity());
            String current_user_id = mCurrentUser.getUid();
            StorageReference filepath = mImageStorage.child("post_images").child(current_user_id + ".jsp");
            filepath.putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if (task.isSuccessful()) {
                        final String dowload_url = task.getResult().getDownloadUrl().toString();
                        mUserDatabase.child("image").setValue(dowload_url);
                    } else {
                        Toast.makeText(getActivity(), "Error in upload!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Post, UserViewHoder> userFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Post, UserViewHoder>(
                Post.class,
                R.layout.single_post_layout,
                UserViewHoder.class,
                mPostUserDatabase

        ) {
            @Override
            protected void populateViewHolder(final UserViewHoder viewHolder, final Post model, int position) {
                viewHolder.setTime(model.getTime_post());
                viewHolder.setStatusPost(model.getPostStatus());
                //viewHolder.setUserPostImage(model.getImage(),getActivity());
                viewHolder.numberLike(model.getCountLike());

                final String list_user_id = getRef(position).getKey();
                mRoot.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String userName = dataSnapshot.child("name").getValue().toString();
                        String userThumb = dataSnapshot.child("thumb_image").getValue().toString();
                        final String email = dataSnapshot.child("email").getValue().toString();

                        viewHolder.setName(userName);
                        viewHolder.setUserImage(userThumb, getContext());


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        count = count + 1;
                        mPostUserDatabase.child(list_user_id).child("countLike").setValue(count);
                    }
                });

            }
        };
        mPostList.setAdapter(userFirebaseRecyclerAdapter);

    }

    public static class UserViewHoder extends RecyclerView.ViewHolder {
        View mView;

        public UserViewHoder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setStatusPost(String date) {
            TextView postStatusView = (TextView) mView.findViewById(R.id.post);
            postStatusView.setVisibility(View.VISIBLE);
            postStatusView.setText(date);
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.user_name_post);
            userNameView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {
            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image_post);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avata).into(userImageView);
        }

        public void setLike() {
            ImageView click = (ImageView) mView.findViewById(R.id.imageViewLike);
        }

        public void numberLike(int like) {
            TextView txtLike = (TextView) mView.findViewById(R.id.txtCountLine);
            txtLike.setText(String.valueOf(like));
        }

        public void setTime(String time) {
            TextView txtTime = (TextView) mView.findViewById(R.id.time);
            txtTime.setText(time);
        }

        public void setUserPostImage(String thumb_image, Context ctx) {
            ImageView userImageView = (ImageView) mView.findViewById(R.id.image_post);
            userImageView.setVisibility(View.VISIBLE);
            Picasso.with(ctx).load(thumb_image).into(userImageView);

        }
    }

}
