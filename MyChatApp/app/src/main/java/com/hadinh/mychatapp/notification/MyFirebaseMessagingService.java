package com.hadinh.mychatapp.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.help.ExtraIntent;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            // Don't show notification if chat activity is open.
        }

        if (remoteMessage.getNotification() != null) {

            String notification_title = remoteMessage.getNotification().getTitle();
            String notification_message = remoteMessage.getNotification().getBody();
            String click_action = remoteMessage.getNotification().getClickAction();
            String from_user_id = remoteMessage.getData().get("from_user_id");
            String current_user_id = remoteMessage.getData().get("recept_user_id");
            String email= remoteMessage.getData().get("email");
            String userName= remoteMessage.getData().get("userName");
           // String notification_type = remoteMessage.getData().get("notification_type");

            NotificationCompat.Builder build = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(notification_title)
                    .setContentText(notification_message)
                    .setAutoCancel(true);


            Intent intern = new Intent(click_action);
            intern.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, from_user_id);
            intern.putExtra(ExtraIntent.EXTRA_CURRENT_USER_ID, current_user_id);
            intern.putExtra("email", email);
            intern.putExtra("name", userName);
            intern.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), new Random().nextInt(), intern, PendingIntent.FLAG_UPDATE_CURRENT);
            build.setContentIntent(resultPendingIntent);
            int mNotificationId = (int) System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(mNotificationId, build.build());
            notificationManager.notify(mNotificationId, build.build());
//                if (FirebaseChatMainApp.isChatActivityOpen()) {
//                    notificationManager.notify(mNotificationId, build.build());
//                    MyChat.setHaveMessage(false);
//                } else {
//                    MyChat.setHaveMessage(true);
//                }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */

}