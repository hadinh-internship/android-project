package com.hadinh.mychatapp.models;

/**
 * Created by intern on 28/07/2017.
 */

public class Post {
    public String postStatus;
    public String datePost;
    public int countLike;
    public String time_post;
    public String image;

    public Post(String postStatus, String datePost, int countLike) {
        this.postStatus = postStatus;
        this.datePost = datePost;
        this.countLike = countLike;
    }

    public Post() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime_post() {
        return time_post;
    }

    public void setTime_post(String time_post) {
        this.time_post = time_post;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getDatePost() {
        return datePost;
    }

    public void setDatePost(String datePost) {
        this.datePost = datePost;
    }

    public int getCountLike() {
        return countLike;
    }

    public void setCountLike(int countLike) {
        this.countLike = countLike;
    }
}
