package com.hadinh.mychatapp.activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.models.User;

import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private EditText mDisplayName;
    private EditText mEmail;
    private EditText mPassword;
    private Button mCreateBtn;
    private Button mGotoLogin;

    private ProgressDialog mRegProgress;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mDisplayName = (EditText) findViewById(R.id.edit_text_display_name);
        mEmail = (EditText) findViewById(R.id.edit_text_email_register);
        mPassword = (EditText) findViewById(R.id.edit_text_password_register);
        mCreateBtn = (Button) findViewById(R.id.btn_register);

        mGotoLogin = (Button) findViewById(R.id.btn_gotoLogin);
        mRegProgress = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String display_name = mDisplayName.getText().toString();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                if (TextUtils.isEmpty(display_name)) {
                    mDisplayName.setError("Enter your display name!");
                } else if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Enter your email!");
                } else if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                    mEmail.setError("Invalid Email Address");
                } else if (TextUtils.isEmpty(password)) {
                    mPassword.setError("Enter your password!");
                } else if (password.length() < 6) {
                    mPassword.setError("Password must more than 6 character!");
                } else {
                    mRegProgress.setTitle("Register User");
                    mRegProgress.setMessage("Please wait while we create your account!");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    mRegProgress.show();
                    register_user(display_name, email, password);
                }


            }
        });
        mGotoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(gotoLogin);
            }
        });
    }

    private void register_user(final String display_name, final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                    String uid = current_user.getUid();

                    Long time = new Date().getTime();

                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                    String device_token = FirebaseInstanceId.getInstance().getToken();

//                    HashMap<String, String> userMap = new HashMap<String, String>();
//
//                    userMap.put("name", display_name);
//                    userMap.put("email", email);
//                    userMap.put("status", "Hi there. I'm use Chat App!");
//                    userMap.put("image", "default");
//                    userMap.put("thumb_image", "default");
//                    userMap.put("device_token", device_token);

                    User user = new User(display_name, "default", "default", "Hi there. I'm use Chat App!", email, time, device_token);


                    mDatabase.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mRegProgress.dismiss();
                                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(mainIntent);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                finish();
                            }
                        }
                    });

                } else {
                    mRegProgress.hide();
                    Toast.makeText(RegisterActivity.this, "Can not signing. Please check the form and try again!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
