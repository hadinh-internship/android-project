package com.hadinh.mychatapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.activitys.LoginActivity;
import com.hadinh.mychatapp.activitys.SettingActivity;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by intern on 18/07/2017.
 */

public class MenuFragment extends Fragment {

    private CircleImageView mAvata;
    private TextView mDisplayName;
    private TextView mStatus;
    private ImageView imageLogout, imageAvavta;
    private TextView mSettingAccount;
    private TextView mTextLogout;
    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);


        mAuth = FirebaseAuth.getInstance();
        String uid = mAuth.getCurrentUser().getUid();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        mAvata = (CircleImageView) view.findViewById(R.id.img_avatar_icon);
        mDisplayName = (TextView) view.findViewById(R.id.txtDisplayName);
        mStatus = (TextView) view.findViewById(R.id.txtStatus);
        imageAvavta = (ImageView) view.findViewById(R.id.imageViewAvata);
        imageLogout = (ImageView) view.findViewById(R.id.imageLogout);
        mSettingAccount = (TextView) view.findViewById(R.id.textAcountSetting);
        mTextLogout = (TextView) view.findViewById(R.id.textLogout);

        imageAvavta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toEditProfile = new Intent(getActivity(), SettingActivity.class);
                startActivity(toEditProfile);
            }
        });
        mSettingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toEditProfile = new Intent(getActivity(), SettingActivity.class);
                startActivity(toEditProfile);
            }
        });

        imageLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toLogin = new Intent(getActivity(), LoginActivity.class);
                logout();
                startActivity(toLogin);
            }
        });
        mTextLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toLogin = new Intent(getActivity(), LoginActivity.class);
                logout();
                startActivity(toLogin);
            }
        });
        showInfor();
        return view;
    }

    public void showInfor() {
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String display_name = dataSnapshot.child("name").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();

                mDisplayName.setText(display_name);
                mStatus.setText(status);
                Picasso.with(mAvata.getContext()).load(image).placeholder(R.drawable.avata_icon).into(mAvata);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void logout() {
        mAuth = FirebaseAuth.getInstance();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
        mUserDatabase.child("online").setValue(ServerValue.TIMESTAMP);
        mUserDatabase.child("device_token").setValue(null);
        mAuth.signOut();
    }

}
