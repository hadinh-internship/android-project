package com.hadinh.mychatapp.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.fragments.FriendsFragment;
import com.hadinh.mychatapp.fragments.HomeFragment;
import com.hadinh.mychatapp.fragments.InforFragment;
import com.hadinh.mychatapp.fragments.MenuFragment;
import com.hadinh.mychatapp.fragments.SearchFragment;
import com.hadinh.mychatapp.fragments.UserFragment;
import com.hadinh.mychatapp.help.ExtraIntent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by intern on 17/07/2017.
 */

public class MainActivity extends AppCompatActivity {
    public static String STR_FRIEND_FRAGMENT = "FRIEND";
    public static String STR_GROUP_FRAGMENT = "GROUP";
    public static String STR_HOME_FRAGMENT = "HOME";
    public static String STR_INFO_FRAGMENT = "INFO";
    public static String STR_SEARCH_FRAGMENT = "SEARCH";
    public static String STR_MENU_FRAGMENT = "MENU";
    public static int Device_Width;
    static String LoggedIn_User_Email;
    private static String TAG = "MainActivity";
    private ViewPager viewPager;
    private TabLayout tabLayout = null;
    private ViewPagerAdapter adapter;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flags) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(flags);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_layout);
//        getSupportActionBar().hide();

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        Device_Width = metrics.widthPixels;

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
            LoggedIn_User_Email = mAuth.getCurrentUser().getEmail();
        } else {
            Intent toStart = new Intent(getApplicationContext(), StartActivity.class);
            startActivity(toStart);
        }

        //initTab();
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.blue_300));
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        initFirebase();
    }

    private void initFirebase() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    ExtraIntent.UID = user.getUid();
                } else {
                    MainActivity.this.finish();
                    // User is signed in
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        ChatActivity.isBackFromChat = true;
        if (currentUser == null) {
            Intent toStart = new Intent(getApplicationContext(), StartActivity.class);
            startActivity(toStart);
        } else {
            mUserRef.child("online").setValue("true");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (ChatActivity.isBackFromChat) {
            if (currentUser != null) {
                mUserRef.child("online").setValue(ServerValue.TIMESTAMP);
            }
        }
    }

    @Override
    protected void onDestroy() {
        //ServiceUtils.startServiceFriendChat(getApplicationContext());
        super.onDestroy();
    }

    /**
     * create 6 tab
     */

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_action_home,
                R.drawable.ic_action_friend,
                R.drawable.ic_action_message,
                R.drawable.ic_information,
                R.drawable.ic_action_search,
                R.drawable.ic_action_menu
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
        tabLayout.getTabAt(5).setIcon(tabIcons[5]);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), STR_HOME_FRAGMENT);
        adapter.addFrag(new UserFragment(), STR_INFO_FRAGMENT);
        adapter.addFrag(new FriendsFragment(), STR_FRIEND_FRAGMENT);
        adapter.addFrag(new InforFragment(), STR_GROUP_FRAGMENT);
        adapter.addFrag(new SearchFragment(), STR_SEARCH_FRAGMENT);
        adapter.addFrag(new MenuFragment(), STR_MENU_FRAGMENT);
        // floatButton.setOnClickListener(((InforFragment) adapter.getItem(0)).onClickFloatButton.getInstance(this));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(6);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (item.getItemId() == R.id.main_logout_btn) {
            FirebaseAuth.getInstance().signOut();
            //sendToStart();
            //logout();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser != null) {
                mUserRef.child("online").setValue(ServerValue.TIMESTAMP);
                // mUserRef.child("lastSeen").setValue(ServerValue.TIMESTAMP);
            }
            startActivity(new Intent(getApplicationContext(), StartActivity.class));
            finish();
        }
        if (item.getItemId() == R.id.main_setting_btn) {
            Intent settingsIntent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(settingsIntent);
        }

        return true;

    }

    /**
     * Adapter show tab
     */
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }
}
