package com.hadinh.mychatapp.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.activitys.ChatActivity;
import com.hadinh.mychatapp.activitys.MyChat;
import com.hadinh.mychatapp.activitys.ProfileActivity;
import com.hadinh.mychatapp.help.ExtraIntent;
import com.hadinh.mychatapp.models.Friends;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by intern on 19/07/2017.
 */

public class FriendsFragment extends Fragment {
    private RecyclerView mFriendsList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;
    private FirebaseAuth mAuth;
    private String mCurrent_user_id;

    private View mMainView;

    public FriendsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.fragment_friends, container, false);

        mFriendsList = (RecyclerView) mMainView.findViewById(R.id.friend_list);

        mAuth = FirebaseAuth.getInstance();

        mCurrent_user_id = mAuth.getCurrentUser().getUid();
        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends").child(mCurrent_user_id);
        mFriendsDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);


        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getActivity()));


        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Friends, FriendsViewHoder> friendsFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Friends, FriendsViewHoder>(
                Friends.class, R.layout.users_single_layout, FriendsViewHoder.class, mFriendsDatabase
        ) {
            @Override
            protected void populateViewHolder(final FriendsViewHoder viewHolder, Friends model, final int position) {
                viewHolder.setDate(model.getDate());
                final String list_user_id = getRef(position).getKey();
                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String userName = dataSnapshot.child("name").getValue().toString();
                        String userThumb = dataSnapshot.child("thumb_image").getValue().toString();
                        final String email = dataSnapshot.child("email").getValue().toString();
                        // String userOnline= dataSnapshot.child("online").getValue().toString();

                        if (dataSnapshot.hasChild("online")) {
                            String userOnline = dataSnapshot.child("online").getValue().toString();
                            viewHolder.setUserOnline(userOnline);
                        }
                        if (MyChat.isChatHaveMessage() == false) {
                            viewHolder.setUserHaveMessage();
                        }
                        viewHolder.setName(userName);
                        viewHolder.setUserImage(userThumb, getContext());


                        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CharSequence options[] = new CharSequence[]{"Open Profile", "Send message"};

                                AlertDialog.Builder build = new AlertDialog.Builder(getContext());
                                build.setTitle("Select Option");
                                build.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {

                                        //click event
                                        if (i == 0) {
                                            Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                                            profileIntent.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, list_user_id);
                                            startActivity(profileIntent);
                                        }
                                        if (i == 1) {
                                            Intent intent = new Intent(getActivity(), ChatActivity.class);
                                            intent.putExtra(ExtraIntent.EXTRA_CURRENT_USER_ID, mCurrent_user_id);
                                            intent.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, list_user_id);
                                            intent.putExtra("email", email);
                                            intent.putExtra("name", userName);

                                            startActivity(intent);
                                        }
                                    }
                                });
                                build.show();
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        };
        mFriendsList.setAdapter(friendsFirebaseRecyclerAdapter);
    }


    public static class FriendsViewHoder extends RecyclerView.ViewHolder {
        View mView;

        public FriendsViewHoder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setDate(String date) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_status);
            userNameView.setText(date);
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_name);
            userNameView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {
            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avata).into(userImageView);
        }

        public void setUserOnline(String online_status) {
            ImageView userOnlineView = (ImageView) mView.findViewById(R.id.user_single_online_icon);
            if (online_status.equals("true")) {
                userOnlineView.setVisibility(View.VISIBLE);
            } else {
                userOnlineView.setVisibility(View.INVISIBLE);
            }
        }

        public void setUserHaveMessage() {
            ImageView imageSetUserHaveMessage = (ImageView) mView.findViewById(R.id.imageSetUserHaveMessage);
            imageSetUserHaveMessage.setVisibility(View.VISIBLE);
        }
    }
}
