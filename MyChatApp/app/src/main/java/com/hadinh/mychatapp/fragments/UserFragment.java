package com.hadinh.mychatapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.activitys.ProfileActivity;
import com.hadinh.mychatapp.activitys.SettingActivity;
import com.hadinh.mychatapp.activitys.StartActivity;
import com.hadinh.mychatapp.help.ExtraIntent;
import com.hadinh.mychatapp.models.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by intern on 20/07/2017.
 */

public class UserFragment extends Fragment {

    private RecyclerView mUsersList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;
    private FirebaseAuth mAuth;
    private String mCurrent_user_id;

    private View mMainView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.activity_list_user, container, false);

        mUsersList = (RecyclerView) mMainView.findViewById(R.id.recycler_view_users);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth == null) {
            Intent toStart = new Intent(getContext(), StartActivity.class);
            startActivity(toStart);
        } else {
            mCurrent_user_id = mAuth.getCurrentUser().getUid();
            mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
            mUsersDatabase.keepSynced(true);
        }


        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(getActivity()));


        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<User, UserViewHoder> userFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<User, UserViewHoder>(
                User.class,
                R.layout.users_single_layout,
                UserViewHoder.class,
                mUsersDatabase
        ) {
            @Override
            protected void populateViewHolder(UserViewHoder viewHolder, User model, int position) {
                viewHolder.setName(model.getName());
                viewHolder.setStatus(model.getStatus());
                viewHolder.setUserImage(model.getThumb_image(), getActivity());
                final String position_user_id = getRef(position).getKey();
                if (position_user_id.equals(mCurrent_user_id)) {
                    viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent toSettingActivity = new Intent(getActivity(), SettingActivity.class);
                            startActivity(toSettingActivity);
                        }
                    });
                } else {
                    viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent toProfile = new Intent(getActivity(), ProfileActivity.class);
                            toProfile.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, position_user_id);
                            startActivity(toProfile);
                        }
                    });
                }

            }
        };
        mUsersList.setAdapter(userFirebaseRecyclerAdapter);

    }


    public static class UserViewHoder extends RecyclerView.ViewHolder {
        View mView;

        public UserViewHoder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setStatus(String date) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_status);
            userNameView.setText(date);
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_name);
            userNameView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {
            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avata).into(userImageView);
        }
    }
}
