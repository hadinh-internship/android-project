package com.hadinh.mychatapp.models;


import com.google.firebase.database.Exclude;

public class User {

    public String name;
    public String image;
    public String thumb_image;
    public String status;
    private String email;
    private long createdAt;
    private String device_token;
    private String post;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    private String mRecipientId;

    public User(String name, String image, String thumb_image, String status, String email, long createdAt, String device_token) {
        this.name = name;
        this.image = image;
        this.thumb_image = thumb_image;
        this.status = status;
        this.email = email;
        this.createdAt = createdAt;
        this.device_token = device_token;
    }

    public User() {
    }

    public User(String name, String image, String thumb_image, String status) {
        this.name = name;
        this.image = image;
        this.thumb_image = thumb_image;
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String createUniqueChatRef(long createdAtCurrentUser, String currentUserEmail){
        String uniqueChatRef="";
        if(createdAtCurrentUser > getCreatedAt()){
            uniqueChatRef = cleanEmailAddress(currentUserEmail)+"-"+cleanEmailAddress(getUserEmail());
        }else {

            uniqueChatRef=cleanEmailAddress(getUserEmail())+"-"+cleanEmailAddress(currentUserEmail);
        }
        return uniqueChatRef;
    }

    private String cleanEmailAddress(String email) {
        //replace dot with comma since firebase does not allow dot
        return email.replace(".", "-");
    }

    private String getUserEmail() {
        //Log.e("user email  ", userEmail);
        return email;
    }

    public String getEmail() {
        return email;
    }


    @Exclude
    public String getRecipientId() {
        return mRecipientId;
    }

    public void setRecipientId(String recipientId) {
        this.mRecipientId = recipientId;
    }
}
