package com.hadinh.mychatapp.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.activitys.ProfileActivity;
import com.hadinh.mychatapp.activitys.SettingActivity;
import com.hadinh.mychatapp.activitys.StartActivity;
import com.hadinh.mychatapp.help.ExtraIntent;
import com.hadinh.mychatapp.models.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by intern on 18/07/2017.
 */

public class SearchFragment extends Fragment {

    SearchView searchView;
    RecyclerView listView;
    private DatabaseReference mUserDatabase;
    private View mMainView;
    private FirebaseAuth mAuth;
    private String mCurrent_user_id;


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.fragment_search, container, false);

        listView = (RecyclerView) mMainView.findViewById(R.id.search_list);
        searchView = (SearchView) mMainView.findViewById(R.id.search);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth == null) {
            Intent toStart = new Intent(getContext(), StartActivity.class);
            startActivity(toStart);
        } else {
            mCurrent_user_id = mAuth.getCurrentUser().getUid();
            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
            mUserDatabase.keepSynced(true);
        }


        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Query query = mUserDatabase.orderByChild("name").startAt(newText).endAt(newText+"\uf8ff");

                FirebaseRecyclerAdapter<User, SearchFragment.UserViewHoder> userFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<User, SearchFragment.UserViewHoder>(
                        User.class,
                        R.layout.users_single_layout,
                        SearchFragment.UserViewHoder.class,
                        query
                ) {
                    @Override
                    protected void populateViewHolder(SearchFragment.UserViewHoder viewHolder, User model, int position) {
                        viewHolder.setName(model.getName());
                        viewHolder.setStatus(model.getStatus());
                        viewHolder.setUserImage(model.getThumb_image(), getActivity());
                        final String position_user_id = getRef(position).getKey();
                        if (position_user_id.equals(mCurrent_user_id)) {
                            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent toSettingActivity = new Intent(getActivity(), SettingActivity.class);
                                    startActivity(toSettingActivity);
                                }
                            });
                        } else {
                            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent toProfile = new Intent(getActivity(), ProfileActivity.class);
                                    toProfile.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, position_user_id);
                                    startActivity(toProfile);
                                }
                            });
                        }

                    }
                };
                listView.getRecycledViewPool().clear();
                listView.setAdapter(userFirebaseRecyclerAdapter);
                return false;
            }
        });

        return mMainView;
    }


    public static class UserViewHoder extends RecyclerView.ViewHolder {
        View mView;

        public UserViewHoder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setStatus(String date) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_status);
            userNameView.setText(date);
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.users_single_name);
            userNameView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {
            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avata).into(userImageView);
        }
    }
}