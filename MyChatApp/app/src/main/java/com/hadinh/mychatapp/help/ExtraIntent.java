package com.hadinh.mychatapp.help;


public final class ExtraIntent {
    public static final String EXTRA_CHAT_REF = "EXTRA_CHAT_REF";
    public static final String EXTRA_CURRENT_USER_ID = "EXTRA_CURRENT_USER_ID";
    public static final String EXTRA_RECIPIENT_ID = "EXTRA_RECIPIENT_ID";
    public static String UID = "";
    public static final String ARG_USERS = "users";
    public static final String ARG_RECEIVER = "receiver";
    public static final String ARG_RECEIVER_UID = "receiver_uid";
    public static final String ARG_CHAT_ROOMS = "chat_rooms";
    public static final String ARG_FIREBASE_TOKEN = "device_token";
    public static final String ARG_FRIENDS = "friends";
    public static final String ARG_UID = "uid";
}
