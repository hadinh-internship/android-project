package com.hadinh.mychatapp.activitys;

import android.app.Application;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by intern on 13/07/2017.
 */

public class MyChat extends Application {
    private static boolean sIsChatActivityOpen = false;
    private static boolean isUserHaveMessage = true;
    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;

    public static boolean isChatActivityOpen() {
        return sIsChatActivityOpen;
    }

    public static void setChatActivityOpen(boolean isChatActivityOpen) {
        MyChat.sIsChatActivityOpen = isChatActivityOpen;
    }

    public static void setHaveMessage(boolean isUserHaveMessage) {
        MyChat.isUserHaveMessage = isUserHaveMessage;
    }

    public static boolean isChatHaveMessage() {
        return isUserHaveMessage;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
            mUserDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        mUserDatabase.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            Intent toStart = new Intent(getApplicationContext(), StartActivity.class);
            toStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(toStart);
        }

    }

}
