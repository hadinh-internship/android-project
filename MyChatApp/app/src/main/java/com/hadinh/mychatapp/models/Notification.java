package com.hadinh.mychatapp.models;

/**
 * Created by intern on 04/08/2017.
 */

public class Notification {
    public String title;
    public String type;
    public String from;

    public Notification() {
    }

    public Notification(String title, String type, String from) {
        this.title = title;
        this.type = type;
        this.from = from;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
