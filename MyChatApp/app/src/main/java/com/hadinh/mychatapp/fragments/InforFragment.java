package com.hadinh.mychatapp.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hadinh.mychatapp.R;
import com.hadinh.mychatapp.activitys.ChatActivity;
import com.hadinh.mychatapp.activitys.ProfileActivity;
import com.hadinh.mychatapp.activitys.SettingActivity;
import com.hadinh.mychatapp.activitys.StartActivity;
import com.hadinh.mychatapp.help.ExtraIntent;
import com.hadinh.mychatapp.models.Notification;
import com.hadinh.mychatapp.models.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class InforFragment extends Fragment {
    private RecyclerView mInforList;
    private DatabaseReference mUserDatabase;
    private DatabaseReference mInforDatabase;
    private FirebaseAuth mAuth;
    private String mCurrent_user_id;

    private View mMainView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.fragment_infor, container, false);

        mInforList = (RecyclerView) mMainView.findViewById(R.id.infor_list);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth == null) {
            Intent toStart = new Intent(getContext(), StartActivity.class);
            startActivity(toStart);
        } else {
            mCurrent_user_id = mAuth.getCurrentUser().getUid();
            mInforDatabase = FirebaseDatabase.getInstance().getReference().child("notification").child(mCurrent_user_id);
            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
            mInforDatabase.keepSynced(true);
        }


        mInforList.setHasFixedSize(true);
        mInforList.setLayoutManager(new LinearLayoutManager(getActivity()));


        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Notification, InforFragment.UserViewHoder> userFirebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Notification, InforFragment.UserViewHoder>(
                Notification.class,
                R.layout.notification_single_layout,
                InforFragment.UserViewHoder.class,
                mInforDatabase
        ) {
            @Override
            protected void populateViewHolder(final InforFragment.UserViewHoder viewHolder, final Notification model, int position) {
                viewHolder.setName(model.getTitle());
                mUserDatabase.child(model.getFrom()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final String name = dataSnapshot.child("name").getValue().toString();
                        String image = dataSnapshot.child("thumb_image").getValue().toString();
                      final  String email= dataSnapshot.child("email").getValue().toString();
                        viewHolder.setStatus(name +" send you one "+ model.getType());
                        viewHolder.setUserImage(image, getActivity());

                        if (model.getTitle().equals("Message")) {
                            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent toChatActivity = new Intent(getActivity(), ChatActivity.class);
                                    toChatActivity.putExtra(ExtraIntent.EXTRA_CURRENT_USER_ID, mCurrent_user_id);
                                    toChatActivity.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, model.getFrom());
                                    toChatActivity.putExtra("email", email);
                                    toChatActivity.putExtra("name", name);
                                    startActivity(toChatActivity);
                                }
                            });
                        } else {
                            viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent toProfile = new Intent(getActivity(), ProfileActivity.class);
                                    toProfile.putExtra(ExtraIntent.EXTRA_RECIPIENT_ID, model.getFrom());
                                    startActivity(toProfile);
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };
        mInforList.setAdapter(userFirebaseRecyclerAdapter);

    }


    public static class UserViewHoder extends RecyclerView.ViewHolder {
        View mView;

        public UserViewHoder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setStatus(String date) {
            TextView userNameView = (TextView) mView.findViewById(R.id.infor_single_status);
            userNameView.setText(date);
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.infor_single_name);
            userNameView.setText(name);
        }

        public void setUserImage(String thumb_image, Context ctx) {
            ImageView userImageView = (ImageView) mView.findViewById(R.id.infor_single_image);
            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avata).into(userImageView);
        }
    }

}
